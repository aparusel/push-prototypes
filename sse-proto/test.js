var path = require("path"),
    url = require("url");
//     myUrl = "http://host.com/this/is/what/i/want/file.jpg",
//     dir = path.dirname(url.parse(myUrl).pathname);
// 
// console.log(dir); // -> /this/is/what/i/want

// req = '/images/chat_bubbles_lime_l.png';
// 
// 
// var favoritemovie = "Titanic";
// 
// switch (favoritemovie) {
// 	case "Titanic": 
// 		{
// 		console.log("Not a bad choice!");
// 		console.log("Not a bad choice!");
// 		break;
// 		}
// 	case "Water World": 
// 		console.log("No comment");
// 		break;
// 	case "Scream 2": 
// 		console.log("It has its moments");
// 		break;
// 	default : console.log("I\'m sure it was great");
// }

var events = require('events'),
	util = require('util');

var Foo = function(initial_no) { this.count = initial_no; };

Foo.prototype = new events.EventEmitter;

Foo.prototype.increment = function() {
	var self = this;
	setInterval(function() {
		if(self.count % 2 === 0) self.emit('even');
		self.count++;
	}, 300);
};

util.puts("hello?");

var lol = new Foo(1);

lol.on('even', function() { 
	util.puts('Number is even! :: ' + this.count);
}).increment();



/* ************************************************************************ */

/* ************************************************************************

*#use(qx.io.remote.transport.XmlHttp)
#use(qx.io.remote.transport.Iframe)
#use(qx.io.remote.transport.Script)

************************************************************************ */