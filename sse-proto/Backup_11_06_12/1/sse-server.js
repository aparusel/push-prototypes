var http = require("http"),
    path = require("path"),
    // fs = require("fs"),
    util = require("util"),
    url = require("url"),
    connections = [],
    history = [],
		id = 0;
    static = require("node-static"),
    file = new static.Server(),
    welcomes = ["Herzlich Willkommen!", "Welcome!", "Bienvenue!",
                "Welkom!", "Velkommen!", "Witamy!", "Vítejte!"],
    EventEmitter = require("events").EventEmitter;

		var pinger;
    
		var Pinger = function() {
      var self = this;
      
      setInterval(function() {
        self.emit("ping")
      }, 5000); 
    };
		
		util.inherits(Pinger, EventEmitter);
		
		var pingListener = function() {
			var data = welcomes[Math.floor(Math.random() * welcomes.length)];
			history[id] = data;
      //console.log("[" + new Date().toLocaleTimeString() + "] " + "PING " + id + ": " + data);
			
			connections.forEach(function(res) {
					res.write("id: " + id + "\n" + 
				            "data: " + data + "\n\n");
			});
			
      id++;
		};
		
		pinger = new Pinger();

    http.createServer(function(req, res) {

      switch (req.url) {

        // if the url is root, redirect to sse-client.html
        case "/": {
          res.writeHead(302, {location: "/sse-client.html"});
          res.end();
        }


        // the event-stream is requested
        case "/events": {
				
				// start Pinging
				if (connections.length == 0) {
					pinger.on("ping", pingListener);
				}

        console.log(req.headers["user-agent"]);
        
          if (req.headers.accept == "text/event-stream") {
            
            res.writeHead(200, {
              'Content-Type': 'text/event-stream',
              'Cache-Control': 'no-cache',
              'Connection': 'keep-alive'
            });
          
            if (req.headers['last-event-id']) {
              var lastId = parseInt(req.headers['last-event-id']);
              
              for(var i = lastId + 1; i < history.length; i++) {
                res.write("event: catchUp" + "\n" +
                          "id: " + i + "\n" + 
                          "data: " + history[i] + "\n\n");
              }
      
            }
          
            connections.push(res);
                    
          }
          
          
         req.on('close', function () {
            removeConnection(res);
          });

          break;
        }

        // client calls back with a POST-request
        case "/ping": {
          //Check if it really is a POST-request
          if (req.method == "POST") {

            var fullBody = '';

            // as long as data come in, append the current chunk of data to the fullBody variable
            req.on('data', function(chunk) {
                fullBody += chunk.toString();
              });

            // request ended, so print the body to the console
            req.on('end', function() {
              res.writeHead(200);
              // res.writeHead(200, "OK", {'Content-Type': 'text/plain'})3;
              res.end();
              //console.log("[" + new Date().toLocaleTimeString() + "] " + fullBody + "\n");

            });
          }

          // if not a POST-Request, response with 404
          else {
            notFound(req, res);
          }

          break;
        }

        /* everything else is assumed to be a request for a static file, 
           including sse-client.html */
        default: {

          path.exists('.'+ req.url, function(exists) {
          // if the requested file exists, serve it  
          if (exists) {
              // console.log("Serving static file: " + req.url);
              file.serve(req, res);
            }
            // if not, response with 404
            else {
              notFound(req, res);
            }
          });
        }
      } 
    }).listen(8000);


    
    
    function removeConnection(res) {
			console.log("*** entered removeConnection() ***");
      var i = connections.indexOf(res);
      if (i !== -1) {
        connections.splice(i, 1);
				console.log("No. Connections: " + connections.length);
      }
			if (connections.length == 0) {
				pinger.removeListener("ping", pingListener);
			}
    }


    function notFound(req, res) {
      console.log("Requested Resource: " + req.url + " not found.");
      res.writeHead(404, {"Content-Type": "text/plain"});
      res.write("Requested Resource: " + req.url + " not found.");
      res.end();
    }


