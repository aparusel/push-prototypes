var history = [],
		id = 0,
		util = require("util"),
		welcomes = ["Herzlich Willkommen!", "Welcome!", "Bienvenue!",
            "Welkom!", "Velkommen!", "Witamy!", "Vítejte!"],
		EventEmitter = require("events").EventEmitter;
		
var Pinger = function() {
    var self = this;
    
    setInterval(function() {
      self.emit("ping")
    }, 5000); 
  };

util.inherits(Pinger, EventEmitter);

var pingListener = function(connections) {
	var data = welcomes[Math.floor(Math.random() * welcomes.length)];
	history[id] = data;
  console.log("[" + new Date().toLocaleTimeString() + "] " + "PING " + id + ": " + data);
	
	connections.forEach(function(res) {
			
			res.write("event: ping" + "\n" + 
								"id: " + id + "\n" + 
		            "data: " + data + "\n\n");
	});
	
    id++;
};

var catchUp = function(res, lastId) {
	
	res.write("event: catchUp" + "\n" +
						"data: Good to have you back!" + "\n" +  
						"data: Here comes what you missed:\n\n");
						
	for(var i = lastId + 1; i < history.length; i++) {
		res.write("event: catchUp" + "\n" + 
							"id: " + i + "\n" +
							"data: " + history[i] + "\n\n");
	}
};

exports.Pinger = Pinger;
exports.pingListener = pingListener;
exports.catchUp = catchUp;



