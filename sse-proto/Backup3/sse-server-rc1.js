var http = require("http"),
		path = require("path"),
    // fs = require("fs"),
    url = require("url"),
		connections = [],
		history = [],
		static = require("node-static"),
		file = new static.Server(),
		welcomes = ["Herzlich Willkommen!", "Welcome!", "Bienvenue!",
							  "Welkom!", "Velkommen!", "Witamy!", "Vítejte!"];
		


		http.createServer(function(req, res) {
			
			
			// // just for debugging
			// console.log(req.url);

			// first if the url is root, change to sse-client.html
			if (req.url =='/') {
					req.url = "/sse-client.html";
			}

			switch (req.url) {


				// Case 1: the event-stream is requested
				case "/events": {

				console.log(req.headers["user-agent"]);
				
					if (req.headers.accept == "text/event-stream") {
						
						res.writeHead(200, {
					    'Content-Type': 'text/event-stream',
					    'Cache-Control': 'no-cache',
					    'Connection': 'keep-alive'
					  });
					
					  if (req.headers['last-event-id']) {
			        var lastId = parseInt(req.headers['last-event-id']);
							
							for(var i = lastId + 1; i < history.length; i++) {
								res.write("event: catchUp" + "\n" +
													"id: " + i + "\n" + 
							            "data: " + history[i] + "\n\n");
							}
			
						}
					
						connections.push(res);
										
					}
					
			   req.on('close', function () {
		        removeConnection(res);
		      });

					break;
				}

				// Case 2: client calls back with a POST-request
				case "/ping": {
					//Check if it really is a POST-request
					if (req.method == "POST") {

						var fullBody = '';

						// as long as data come in, append the current chunk of data to the fullBody variable
					  req.on('data', function(chunk) {
					      fullBody += chunk.toString();
					    });

						// request ended, so print the body to the console
					  req.on('end', function() {
							res.writeHead(200);
					    // res.writeHead(200, "OK", {'Content-Type': 'text/plain'})3;
					    res.end();
							console.log("[" + new Date().toLocaleTimeString() + "] " + fullBody + "\n");

					  });
					}

					// if not a POST-Request, response with 404
					else {
						notFound(req, res);
					}

					break;
				}

				/* Case 3: everything else is assumed to be a request for a static file, 
				   including sse-client.html */
				default: {

					path.exists('.'+ req.url, function(exists) {
					// if the requested file exists, serve it  
					if (exists) {
					    console.log("Serving static file: " + req.url);
						  file.serve(req, res);
					  }
						// if not, response with 404
						else {
							notFound(req, res);
						}
					});
				}
			}	
		}).once('request', function(){startPinging();}).listen(8000);


		function startPinging() {

			id = 0;
			var data;

			//res.write("retry: 5000");

			setInterval(function() {
				data = welcomes[Math.floor(Math.random() * welcomes.length)];
				
				history[id] = data;
				
				console.log("[" + new Date().toLocaleTimeString() + "] " + "PING " + id + ": " + data);
				
				connections.forEach(function(res) {
						res.write("id: " + id + "\n" + 
					            "data: " + data + "\n\n");
				});
				id++;
			  }, 2000);
		}
		
		function removeConnection(res) {
		  var i = connections.indexOf(res);
		  if (i !== -1) {
		    connections.splice(i, 1);
		  }
		}


		function notFound(req, res) {
			console.log("Requested Resource: " + req.url + " not found.");
		  res.writeHead(404, {"Content-Type": "text/plain"});
		  res.write("Requested Resource: " + req.url + " not found.");
		  res.end();
		}


