var http = require("http"),
    path = require("path"),
    // fs = require("fs"),
		connections = [],
    util = require("util"),
    url = require("url"),
    static = require("node-static"),
    file = new static.Server(),
		ping = require("./ping");
		
		var pinger = new ping.Pinger();

		
		var pingListener = function () {
			ping.pingListener(connections);
		};
		

		

    var http = http.createServer(function(req, res) {
			
      switch (req.url) {

        // if the url is root, redirect to ws-client.html
        case "/": {
          res.writeHead(302, {location: "/ws-client.html"});
          res.end();
        }

        /* everything else is assumed to be a request for a static file, 
           including ws-client.html */
        default: {

          path.exists('.'+ req.url, function(exists) {
          // if the requested file exists, serve it  
          if (exists) {
              // console.log("Serving static file: " + req.url);
              file.serve(req, res);
            }
            // if not, response with 404
            else {
              notFound(req, res);
            }
          });
        }
      } 
    }).listen(8000);

		// extend the existing http-Server so that
		// Web Sockets are enabled at /events
		var io = require("socket.io").listen(http);

		io.sockets.on('connection', function(ws) {
					
				if (connections.length == 0) {
					pinger.on("ping", pingListener);
				}
				connections.push(ws);

		    ws.on('message', function(message) {
		        console.log("[" + new Date().toLocaleTimeString() + "] " + 
												'PONG: %s', message);
		    });
				ws.on('disconnect', function (code, message){
					removeConnection(ws);
				});
		});


    
    
    function removeConnection(res) {
			console.log("*** entered removeConnection() ***");
      var i = connections.indexOf(res);
      if (i !== -1) {
        connections.splice(i, 1);
				console.log("No. Connections: " + connections.length);
      }
			if (connections.length == 0) {
				pinger.removeListener("ping", pingListener);
			}
    }


    function notFound(req, res) {
      console.log("Requested Resource: " + req.url + " not found.");
      res.writeHead(404, {"Content-Type": "text/plain"});
      res.write("Requested Resource: " + req.url + " not found.");
      res.end();
    }

		//optional for debugging
		function debugHeaders(req) {
		  util.puts('URL: ' + req.url);
		  for (var key in req.headers) {
		    util.puts(key + ': ' + req.headers[key]);
		  }
		  util.puts('\n\n');
		}

		


