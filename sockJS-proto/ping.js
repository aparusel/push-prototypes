
var	util = require("util"),
		welcomes = ["Herzlich Willkommen!", "Welcome!", "Bienvenue!",
            "Welkom!", "Velkommen!", "Witamy!", "Vítejte!"],
		EventEmitter = require("events").EventEmitter;
		
var Pinger = function() {
    var self = this;
    
    setInterval(function() {
      self.emit("ping")
    }, 5000); 
  };

util.inherits(Pinger, EventEmitter);

var pingListener = function(connections) {
	var data = welcomes[Math.floor(Math.random() * welcomes.length)];
  console.log("[" + new Date().toLocaleTimeString() + "] " + "PING: " + data);
	
	connections.forEach(function(ws) {
		ws.send(data);
	});
};


exports.Pinger = Pinger;
exports.pingListener = pingListener;




